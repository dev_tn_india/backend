<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();


Route::get('/no', function () {
    return view('errors.404');
});
Route::group(['middleware' => ['auth', 'admin']], function()
{
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/users', 'UserController@index')->name('users');

Route::get('/tickets', 'TicketController@index')->name('tickets');
Route::post('/ticket', 'TicketController@store')->name('ticket');
Route::post('/ticket/{ticket}/replies', 'ReplyController@store')->name('reply');
Route::get('/ticket/delete/{id}', 'TicketController@destroy');

Route::post('/service', 'ServiceController@store')->name('service');
Route::get('/services', 'ServiceController@create')->name('createservice');
Route::get('/service/delete/{id}', 'ServiceController@destroy')->name('destroyservice');

Route::get('/history', 'HistoryController@index')->name('history');
Route::post('/package', 'PackageController@store')->name('pack');
Route::get('/packages/delete/{id}', 'PackageController@destroy')->name('packremove');


Route::get('/rates', 'RateController@index')->name('rates');
Route::post('/rates', 'RateController@store')->name('addrates');
Route::get('/rates/delete/{id}', 'RateController@destroy')->name('removerates');


Route::get('/user/{id}', 'UserController@show')->name('usershow');
Route::post('/update/user', 'UserController@edit')->name('userupdate');

Route::get('user/delete/{id}', 'UserController@destroy');
Route::get('tips', 'TipController@index');
Route::post('tips', 'TipController@store');
Route::get('tips/delete/{id}', 'TipController@destroy');
});


Route::post('/user', 'UserController@store')->name('user');
/*Route::get('/services', 'ServiceController@index')->name('services');
 Route::get('/services', 'ServiceController@index')->name('services');
Route::get('/services', 'ServiceController@index')->name('services');
Route::get('/services', 'ServiceController@index')->name('services');*/
