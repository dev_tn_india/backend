@extends('admin.admin_template')
@section('content')
	<div class="row" id="tickets-list">
		<div class="col-md-8">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Tickets List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Sender</th>
                  <th>Ticket</th>
                  <th>Received at</th>
                  <th>Reply</th>
                  <th>Show details</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($tickets))
 					    #No data
              @else
                @foreach ($tickets as $ticket)
	                <tr>
	                  <td># {{ $ticket->id }}</td>
	                  <td>{{$ticket->user->name}}</td>
                    <td>{{ $ticket->content }}</td>
                    <td>{{$ticket->created_at->toDayDateTimeString()}}</td>
                    <td><a href="ticket/{{ $ticket->id }}" class="reply">Reply</a></td>
	                  <td><a href="ticket/{{ $ticket->id }}" class="details">Details</a></td>
	                  <td>
  	                  <form method="POST" action="tickets/{{ $ticket->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="button" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Sender</th>
                  <th>Ticket</th>
                  <th>Received at</th>
                  <th>Reply</th>
                  <th>Show details</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
    <div class="col-md-4">
                <!-- Ticket box -->
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-comments-o"></i>
              <h3 class="box-title">Ticket</h3>
            </div>
            <div class="box-body chat" id="chat-box">
              <!-- chat item -->
              <div class="item">
                <img src="{{ asset("/node_modules/admin-lte/dist/img/user2-160x160.jpg")}}" alt="user image" class="offline">

                <p class="message">
                  <a href="#" class="name" >
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <span id="msgtime">5:30</span></small>
                    <span id="customername">Susan Doe</span>
                  </a>
                 <span id="msgcontent">
                  I would like to meet you to discuss the latest news about
                  the arrival of the new theme. They say it is going to be one the
                  best themes on the market
                </span>
                </p>
              </div>
              <!-- /.item -->
            </div>
            <!-- /.chat -->
            <div class="box-footer">
              <div class="input-group">
                <input class="form-control" placeholder="Type message...">

                <div class="input-group-btn">
                  <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box (chat box) -->
    </div>  

	</div>
@endsection
