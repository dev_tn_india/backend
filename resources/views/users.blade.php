@extends('admin.admin_template')
@section('content')
	<div class="row" id="users-list">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Service</th>
                  <th>Package</th>
                  <th>Active</th>
                  <th>Account type</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($users))
 					    #No data
              @else
                @foreach ($users as $user)
	                <tr>
	                  <td># {{ $user->id }}</td>
	                  <td>{{ $user->name }}</td>
	                  <td>{{$user->email}}</td>
	                  <td>{{$user->phone}}</td>
                    <td>
                      @foreach ($user->servicespackages as $product)
                        @foreach($product->servicesx as $detail)
                          {{$detail->service->name}}
                       <br>
                        @endforeach
                      @endforeach
                    </td>
                    <td>
                       @foreach ($user->servicespackages as $product)
                        @foreach($product->servicesx as $detail)
                          {{$detail->package->duration}}
                       <br>
                        @endforeach
                      @endforeach
                    </td>
                    <td>{{$user->active == 1? 'active': 'pending'}}</td>
	                  <td>{{$user->type ==1 ? 'Trial' : 'Premieum' }}</td>
	                  <td>{{$user->created_at->toDayDateTimeString()}}</td>
	                  <td>
  	                  <form method="POST" action="users/{{ $user->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="button" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Service</th>
                  <th>Package</th>
                  <th>Active</th>
                  <th>Account type</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>
@endsection
