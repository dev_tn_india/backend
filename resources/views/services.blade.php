@extends('admin.admin_template')
@section('content')
	<div class="row" id="services-list">

            <div class="col-md-6">
                <!-- tip box -->
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-comments-o"></i>
              <h3 class="box-title">Service</h3>
            </div>
            <div class="box-body" >
              <form method="post" action="{{action('ServiceController@store')}}">
                        {{ csrf_field() }}

                 <div class="row">
                <div class="col-md-12">
                  <label>Name</label>
                  <input type="text" class="form-control" placeholder="name" name="name">
                </div>
                <div class="col-md-12">
                  <label>Description</label>
                  <textarea class="form-control" name="description" placeholder="description" rows="3"></textarea>
                </div>

                <div class="col-md-12">
                  <label>Select packages</label>
                  <select class="form-control" name="packages[]" multiple>
                    @foreach($packages as $package)
                      <option value="{{$package->id}}">{{$package->duration}} - {{$package->price}} INR</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                      <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
                  
                </div>
              </div>
              </form>
            </div>
            <!-- /.service -->
            <div class="box-footer">
            </div>
          </div>
          <!-- /.box (service box) -->
    </div> 

            <div class="col-md-6">
                <!-- tip box -->
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-comments-o"></i>
              <h3 class="box-title">Package</h3>
            </div>
            <div class="box-body" >
              <form method="post" action="{{action('PackageController@store')}}">
                        {{ csrf_field() }}

                 <div class="row">
                <div class="col-md-12">
                                    <div class="input-group">
                     <label>Duration</label>
                  <input type="number" class="form-control" placeholder="duration" name="duration"> months
          
                  </div>

                   </div>
                <div class="col-md-12">
                  <label>Price</label>
                  <input type="text" class="form-control" placeholder="price" name="price">
                </div>
                <div class="col-md-3">
                      <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
                  
                </div>
              </div>
              </form>
            </div>
            <!-- /.service -->
            <div class="box-footer">
              <ul>
                @foreach($packages as $pack)
                  <li>{{$pack->duration}} - {{$pack->price}} INR 

                    <form method="get" action="packges/delete/{{ $pack->id }}" class="delpage" style="    display: inline-block;">
                       {{ csrf_field() }}
                      <input name="_method" type="hidden" value="DELETE">
                        <button type="submit" class="delbtn">
                          <i class="fa fa-trash"></i>
                        </button>
                      </form>

                    </li>
                @endforeach
              </ul>
            </div>
          </div>
          <!-- /.box (service box) -->
    </div> 

		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Services List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Services</th>
                  <th>Description</th>
                  <th>Duration</th>
                  <th>price</th>
                  <th><i class="fa fa-edit"></i></th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($services))
 					    #No data
              @else
                @foreach ($services as $service)
	                <tr>
	                  <td># {{ $service->id }}</td>
	                  <td>{{$service->name}}</td>
                    <td>{{ $service->description }}</td>
                    <td>


@foreach ($service->packages as $product)
    - {{$product->duration}} <br>
@endforeach

                    </td>
	                  <td>
                    
@foreach ($service->packages as $product)
    - {{$product->price}} INR <br>
@endforeach

         
         </td>
	                  <td> <i class="fa fa-edit"></i></td>
	                  <td>
  	                  <form method="POST" action="services/{{ $service->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="submit" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Services</th>
                  <th>Description</th>
                  <th>Duration</th>
                  <th>price</th>
                  <th><i class="fa fa-edit"></i></th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>


	</div>
@endsection
