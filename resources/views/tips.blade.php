@extends('admin.admin_template')
@section('content')
	<div class="row" id="tips-list">
        <div class="col-md-12">
                <!-- tip box -->
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-comments-o"></i>
              <h3 class="box-title">Tips</h3>
            </div>
            <div class="box-body" >
              <form method="post" action="{{action('TipController@store')}}">
                        {{ csrf_field() }}

                 <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control" placeholder="tip" name="content">
                </div>
                <div class="col-md-3">
                  <select class="form-control" name="service_id">
                    @foreach($services as $service)
                      <option value="{{$service->id}}">{{$service->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <div class="input-group">
                      <input type="date" class="form-control" name="show_at">
                    <div class="input-group-btn">
                      <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <!-- /.chat -->
            <div class="box-footer">
            </div>
          </div>
          <!-- /.box (chat box) -->
    </div>  

		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Tips List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Content</th>
                  <th>Service</th>
                  <th>Show at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($tips))
 					    #No data
              @else
                @foreach ($tips as $tip)
	                <tr>
	                  <td># {{ $tip->id }}</td>
                    <td>{{ $tip->content }}</td>
	                  <td>{{ $tip->service->name }}</td>
	                  <td>{{$tip->show_at}}</td>
	                  <td>
  	                  <form method="get" action="tips/delete/{{ $tip->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="submit" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Content</th>
                  <th>Service</th>
                  <th>Show at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>


	</div>
@endsection
