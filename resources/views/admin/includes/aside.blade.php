<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/node_modules/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ setActive('/home') }}"><a href="{{action('HomeController@index')}}"><span>Dashboard</span></a></li>
            <li {{ setActive('users') }}><a href="{{action('UserController@index')}}"><span>Users</span></a></li>
            <li {{ setActive('services') }}><a href="{{action('ServiceController@create')}}"><span>Services</span></a></li>
            <li {{ setActive('tips') }}><a href="{{action('TipController@index')}}"><span>Tips</span></a></li>
            <li {{ setActive('tickets') }}><a href="{{action('TicketController@index')}}"><span>Tickets</span></a></li>
            <li {{ setActive('rates') }}><a href="{{action('RateController@index')}}"><span>Success Rate</span></a></li>
            <li {{ setActive('history') }}><a href="{{action('HistoryController@index')}}"><span>History</span></a></li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>