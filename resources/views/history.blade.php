@extends('admin.admin_template')
@section('content')
	<div class="row" id="tips-list">
        

		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">History List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>user</th>
                  <th>servive</th>
                  <th>package</th>
                  <th>pay type</th>
                  <th>proof</th>
                  <th>operation</th>
                  <th>created at</th>
                </tr>
                </thead>
                <tbody>
              @if(empty($traces))
 					    #No data
              @else
                @foreach ($traces as $trace)
	                <tr>
	                  <td># {{ $trace->id }}</td>
                    <td>{{ $trace->user()->name }}</td>
                    <td>{{ $trace->service()->name }}</td>
                    <td>{{ $trace->package()->duration }}</td>
	                  <td>{{$trace->paytype}}</td>
	                  <td>{{$trace->proof}}</td>
	                  <td>{{$trace->operation}}</td>
	                  <td>{{$trace->created_at}}</td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>user</th>
                  <th>servive</th>
                  <th>package</th>
                  <th>pay type</th>
                  <th>proof</th>
                  <th>operation</th>
                  <th>created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>


	</div>
@endsection
