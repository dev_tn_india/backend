<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->call(UserTableSeeder::class);
       $this->call(TicketTableSeeder::class);
        factory(App\Service::class, 10)->create();
        factory(App\Package::class, 5)->create();
        factory(App\Tip::class, 10)->create();
        factory(App\Reply::class, 12)->create();

       
    }
}
