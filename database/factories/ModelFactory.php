<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->password,
        'phone' => $faker->unique()->bankRoutingNumber,
        'address' => $faker->unique()->address,
        'type' => $faker->numberBetween(0,2),
        'active' => $faker->numberBetween(0,2),
        'is_admin' => 0,
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Ticket::class, function (Faker\Generator $faker) {

    return [
        'content' => $faker->text,
        'status' => 0,
        'user_id' => $faker->numberBetween(1,20),
    ];
});

$factory->define(App\Service::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->words(6,true)  ,
        'service_id' => $faker->numberBetween(1,10),
        'description' => $faker->text,
    ];
});

$factory->define(App\Package::class, function (Faker\Generator $faker) {

    return [
        'duration' => $faker->numberBetween(1,12)." months",
        'price' => $faker->randomNumber(4),
    ];
});

$factory->define(App\Tip::class, function (Faker\Generator $faker) {

    return [
        'content' => $faker->paragraph(4, true),
        'show_at' => $faker->date('Y-m-d', 'now'),
    ];
});

$factory->define(App\Reply::class, function (Faker\Generator $faker) {

    return [
        'content' => $faker->paragraph(4, true),
        'ticket_id' => $faker->numberBetween(1,10),
    ];
});
