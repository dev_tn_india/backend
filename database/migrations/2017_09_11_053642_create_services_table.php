<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });
        
        Schema::create('package_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id');
            $table->integer('service_id');
           // $table->primary(['package_id', 'service_id']);
        });

/*****************Eager Loads ************** attach... **/
        Schema::create('service_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('package_service_id');
            $table->date('start_at');
            $table->date('end_at');
            $table->tinyInteger('state'); //1:payment is active, 0:end of period 
         //   $table->primary(['user_id', 'package_service_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
        Schema::dropIfExists('package_service');
        Schema::dropIfExists('service_user');

    }
}
