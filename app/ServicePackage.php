<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePackage extends Model
{
    //
	protected $table = 'package_service';


public function service() {
	return $this->belongsTo(Service::class);
}

public function package() {
	return $this->belongsTo(Package::class);
}
}
