<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Package;

class Service extends Model
{
    public function packages() {
        return $this->belongsToMany(Package::class);
    }
}
