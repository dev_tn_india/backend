<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ServicePackageUser extends Model
{

	protected $table = 'service_user';

    public function users() {

    	return $this->belongsTo(User::class, 'service_user', 'package_service_id','user_id');
    }

    public function servicesx() {

    	return $this->hasMany(ServicePackage::class, 'id');	
    }
}
