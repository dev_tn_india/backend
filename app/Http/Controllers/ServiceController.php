<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Package;
class ServiceController extends Controller
{
    public function index() {
		$services = Service::all();
    	return view('services', compact('services'));
    }

    public function create() {
        $services = Service::all();
		$packages = Package::all();
		return view('services', compact('services', 'packages'));
	}

     public function store(Request $request)
    {

           $this->validate($request, [
            'name' => 'required|string|min:2|unique:services',
            'description' => 'string',
            ]);

//return $request->packages;
            $service = new Service;
            $service->name = $request->name;
            $service->description = $request->description;
            $service->save();
            //attach array
                $service->packages()->attach($request->packages);
         
            
			
	        return back();

    }


      public function update(Request $request, $id)
    {
          
            $service = Service::findOrFail($id);
            $service->name = $request->name;
            $service->description = $request->description;
            $service->packages()->attach($request->packages);
            $service->save();

            return back();

    }


	public function destroy($id)
    {
    		$service =  Service::findOrFail($id);
            $service->packages()->detach();
        	$service->delete();
        return back();
    }
    
}
