<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tip;
use App\Service;

class TipController extends Controller
{

   public function index() {
   	$tips = Tip::all();

	$services = Service::all();
   	return view('tips', compact('tips', 'services'));
   }

public function store(Request $request)
    {
           $this->validate($request, [
            'content' => 'required|string|min:2',
            'show_at' => 'required|date',
            ]);

            $tip = new Tip;
            $tip->content = $request->content;
            $tip->service_id = $request->service_id;
            $tip->show_at = $request->show_at;
            $tip->save();

            return back();

    }

	public function destroy($id)
    {
        Tip::findOrFail($id)->delete()? "deleted" : "problem";
        return back();
    }
}
