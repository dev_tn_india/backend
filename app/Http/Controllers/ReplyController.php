<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function store(Ticket $ticket, Request $request)
    {  			
           	$this->validate($request, [
            'content' => 'required|string|min:2',
            ]);
           /* $reply = new Reply;
            $reply->content */
            $ticket->replies()->create(['content' => $request->content,]);
	        return back();
    }
}
