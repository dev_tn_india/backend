<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;
class RateController extends Controller
{
        public function index() {
        	$rates = Rate::all();
    	return view('rates', compact('rates'));
    }
	public function store(Request $request) {
		 $this->validate($request, [
            'rate' => 'required|integer',
            ]);

        	$rate = new Rate;
        	$rate->rate = $request->rate;
        	$rate->save();
    	return back();
    }
    
    public function destroy($id)
    {
        Rate::findOrFail($id)->delete()? "deleted" : "problem";
        return back();
    }
}
