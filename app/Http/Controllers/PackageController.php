<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
class PackageController extends Controller
{



public function store(Request $request)
    {
           $this->validate($request, [
            'duration' => 'required|integer',
            'price' => 'required|integer',
            ]);

            $pack = new Package;
            $pack->duration = $request->duration;
            $pack->price = $request->price;
            $pack->save();

            return back();

    }


	public function destroy($id)
    {
        Package::findOrFail($id)->delete()? "deleted" : "problem";
        return back();
    }
}
