<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
class HistoryController extends Controller
{
     
    public function index() {
    	$traces = History::all();
    	return view('history', compact('traces'));
    }
}
