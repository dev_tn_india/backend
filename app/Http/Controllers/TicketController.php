<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
class TicketController extends Controller
{
      public function index()
    {
        $tickets = Ticket::all();
        $tit = Ticket::find(1);
        return view('tickets', compact('tickets', 'tit'));
    }


      public function store(Request $request)
    {
           $this->validate($request, [
            'content' => 'required|string|min:2',
            'user_id' => 'required',
            ]);

            $ticket = new Ticket;
            $ticket->content = $request->content;
            $ticket->user_id = $request->user_id;
            $ticket->save();

            return back();

    }


      public function update(Request $request, $id)
    {
          
            $ticket = Ticket::findOrFail($id);
            $ticket->status = $request->status;
            $ticket->save();

            return back();

    }


	public function destroy($id)
    {
        Ticket::findOrFail($id)->delete()? "deleted" : "problem";
        return back();
    }

}
